<?php

namespace Yongf\Utils;

/**
 * 数组相关
 */
class UArray
{
    /**
     * @Description：数组以key的值返回数据  value=>data
     *
     *  key = id 时
     * [["id":1,"name"=>"yf"],["id":2,"name"=>"zf"]]  ==>  [1:["id":1,"name"=>"yf"],2:["id":2,"name"=>"zf"]]
     *
     * @Docs：
     *
     * @param array $array
     * @param       $key
     * @param array $keepKeyArray ["id","name"]  如果count($keepKeyArray) == 1 返回 [1:"yf"]
     *
     * @return array
     * @Author：雍飞
     * @Date：2022/6/23 10:48
     */
    public static function arrayRebuild(array $array, $key, array $keepKeyArray = []): array
    {
        $data = array();

        if (empty($array) || empty($key)) {
            return $data;
        }

        $keepKeyArray = array_flip($keepKeyArray);

        foreach ($array as $info) {
            if (isset($info[$key])) {
                if (empty($keepKeyArray)) {
                    $data[$info[$key]] = $info;
                } else {
                    if (count($keepKeyArray) > 1) {
                        $data[$info[$key]] = array_intersect_key($info, $keepKeyArray);
                    } else {
                        $data[$info[$key]] = $info[key($keepKeyArray)];
                    }
                }
            }
        }
        return $data;
    }


    /**
     * @Description：数组以key的值进行分组返回  value=>[data1,data2]
     * key = type 时
     * [["id":1,"name"=>"yf","type":1],["id":2,"name"=>"zf","type":1]]  ==>  [1:[["id":1,"name"=>"yf"],["id":2,"name"=>"zf"]]]
     * @Docs：
     *
     * @param array $array
     * @param       $key
     * @param array $keepKeyArray
     *
     * @return array
     * @Author：雍飞
     * @Date：2022/6/23 10:56
     */
    public static function arrayGroup(array $array, $key, array $keepKeyArray = []): array
    {
        $data = array();

        if (empty($array) || empty($key)) {
            return $data;
        }

        $keepKeyArray = array_flip($keepKeyArray);

        foreach ($array as $info) {
            if (isset($info[$key])) {
                if (empty($keepKeyArray)) {
                    $data[$info[$key]][] = $info;
                } else {
                    if (count($keepKeyArray) > 1) {
                        $data[$info[$key]][] = array_intersect_key($info, $keepKeyArray);
                    } else {
                        $data[$info[$key]][] = $info[key($keepKeyArray)];
                    }
                }
            }
        }

        return $data;
    }


    /**
     * @Description：多维数组排序
     * @Docs：
     *
     * @param     $arr
     * @param     $key
     * @param int $sort
     *
     * @return mixed|void
     * @Author：雍飞
     * @Date：2022/6/23 17:22
     */
    public static function multisort(&$arr, $key, int $sort = SORT_ASC)
    {
        if (empty($arr)) {
            return $arr;
        }
        if (count($arr) == count($arr, 1)) {
            // 一维数组
            array_multisort($arr, $sort);
        } else {
            // 多维数组
            $kArr = array_column($arr, $key);
            if (!empty($kArr)) {
                array_multisort($kArr, $sort, $arr);
            }
        }
    }

}