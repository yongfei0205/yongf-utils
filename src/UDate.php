<?php

namespace Yongf\Utils;

class UDate
{

    /**
     * @Description：获取 一段时间的 每周的开始结束时间
     * @Docs：
     *
     * @param $startDate
     * @param $endDate
     *
     * @return array
     * @Author：雍飞
     * @Date：2022/6/23 14:36
     */
    public static function getWeekStartDay($startDate, $endDate): array
    {
        $arr = [];
        //参数不能为空
        if (!empty($startDate) && !empty($endDate)) {
            //先把两个日期转为时间戳
            $startDate = strtotime($startDate);
            $endDate   = strtotime($endDate);
            //开始日期不能大于结束日期
            if ($startDate <= $endDate) {
                $end_date = strtotime("next monday", $endDate);
                if (date("w", $startDate) == 1) {
                    $start_date = $startDate;
                } else {
                    $start_date = strtotime("last monday", $startDate);
                }
                //计算时间差多少周
                $countWeek = ($end_date - $start_date) / (7 * 24 * 3600);
                for ($i = 0; $i < $countWeek; $i++) {
                    $sd         = date("Y-m-d", $start_date);
                    $ed         = strtotime("+ 6 days", $start_date);
                    $eed        = date("Y-m-d", $ed);
                    $arr[]      = array($sd, $eed);
                    $start_date = strtotime("+ 1 day", $ed);
                }
            }
        }
        return $arr;
    }

    /**
     * @Description：当前毫秒
     * @Docs：
     *
     * @return float|int
     * @Author：雍飞
     * @Date：2022/6/23 15:25
     */
    public static function currentMicroTime()
    {
        return microtime(true) * 1000;
    }

    /**
     * @Description：消耗总时长
     * @Docs：
     *
     * @param        $startTime
     * @param bool   $displayUnit
     *
     * @return float|int
     * @Author：雍飞
     * @Date：2022/6/23 15:20
     */
    public static function consumeTotalTime($startTime, bool $displayUnit = true)
    {
        $time = (microtime(true) * 1000 - $startTime);
        if ($time <= 0) {
            return $time;
        }
        $unit = "毫秒";
        if ($time > 1000) {
            $unit = "秒";
            $time = UFunc::toFixed($time / 1000);
        } else {
            $time = UFunc::toFixed($time);
        }

        return $displayUnit ? $time . strtolower($unit) : $time;
    }

    /**
     * @Description：获取当前格式化后的时间
     * @Docs：
     *
     * @param string $format
     *
     * @return false|string
     * @Author：雍飞
     * @Date：2022/6/23 16:57
     */
    public static function currentDate(string $format = "Y-m-d H:i:s")
    {
        if (empty($format)) {
            $format = "Y-m-d H:i:s";
        }
        return date($format);
    }
}